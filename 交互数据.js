// 获取所有的station以及当前下面的rfid  GET
{url: '...'};
output: [
    {
        id: 1,
        name: 'station1',
        rfid: [
            {
                id: 1,
                name: 'rfid1'
            }, {
                id: 2,
                name: 'rfid2'
            }
        ]
    }, {
        id: 2,
        name: 'station2',
        rfid: [
            {
                id: 3,
                name: 'rfid3'
            }
        ]
    }, {
        id: 3,
        name: 'station3',
        rfid: [
            {
                id: 4,
                name: 'rfid4'
            }
        ]
    }
]
// 获取所有的rfid的路径  GET
{url: '...'};
output: [
    {
        id: 1,
        name: 'rfid1',
        route: [
            {
                stationId: 1,
                time: '13:00'
            }, {
                stationId: 2,
                time: '12:30'
            }, {
                stationId: 3,
                time: '12:00'
            }
        ]
    }, {
        id: 2,
        name: 'rfid2',
        route: [
            {
                stationId: 1,
                time: '13:00'
            }, {
                stationId: 3,
                time: '12:30'
            }, {
                stationId: 2,
                time: '12:00'
            }
        ]
    }, {
        id: 3,
        name: 'rfid3',
        route: [
            {
                stationId: 2,
                time: '13:00'
            }, {
                stationId: 3,
                time: '12:40'
            }, {
                stationId: 1,
                time: '12:20'
            }
        ]
    }, {
        id: 4,
        name: 'rfid4',
        route: [
            {
                stationId: 3,
                time: '13:00'
            }, {
                stationId: 1,
                time: '12:40'
            }, {
                stationId: 2,
                time: '12:20'
            }
        ]
    }
]
// 获取rfid1的路径route GET
{url: '...'};
output: {
    id: 1,
    name: 'rfid1',
    route: [
        {
            stationId: 1,
            time: '13:00'
        }, {
            stationId: 2,
            time: '12:30'
        }, {
            stationId: 3,
            time: '12:00'
        }
    ]
}
// 增加一个station POST
{url: '...'};
input: {
    id: 4,
    name: 'station4',
    rfid: [
    ]
}
//删除一个station DELETE
{url: '...'};