function myFactory($window){
    var service = {
        // 本地储存单个值
        setStorage: function(key, value){
            $window.localStorage[key] = value;
        },
        // 读取本地存储单个值
        getStorage: function(key){
            return  $window.localStorage[key];
        },
        // 本地存储对象
        setObjStorage: function(key,value){
            $window.localStorage[key] = JSON.stringify(value);
        },
        // 读取本地存储对象
        getObjStorage: function(key){
            return JSON.parse($window.localStorage[key] || '{}');
        },
        // cookie设置
        setCookie: function(cname,cvalue,exdays){
            var d = new Date();
            d.setTime(d.getTime()+(exdays*24*60*60*1000));
            var expires = "expires="+d.toGMTString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        },
        //cookie读取
        getCookie: function(cname){
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) 
            {
                var c = ca[i].trim();
                if (c.indexOf(name)==0) return c.substring(name.length,c.length);
            }
            return "";
        },

        checkCookie: function(){
            var user=getCookie("username");
            if (user!="")
            {
                alert("Welcome again " + user);
            }else{
                user = prompt("Please enter your name:","");
                if (user!="" && user!=null)
                {
                setCookie("username",user,365);
                }
            }
        },

        // 获取对应所有的station以及当前下面的rfid
        getStation: function(){
            return {
                data: [
                    {
                        id: 1,
                        name: 'station1',
                        rfid: [
                            {
                                id: 1,
                                name: 'rfid1'
                            },{
                                id: 2,
                                name: 'rfid2'
                            }
                        ]
                    },{
                        id: 2,
                        name: 'station2',
                        rfid: [
                            {
                                id: 3,
                                name: 'rfid3'
                            }
                        ]
                    },{
                        id: 3,
                        name: 'station3',
                        rfid: [
                            {
                                id: 4,
                                name: 'rfid4'
                            }
                        ]
                    }
                ]
            }
        },
        // 在getStation的基础上增加一个station
        addStation: function(datas){
            datas.push({
                id: datas.length + 1,
                name: 'station' + (datas.length + 1),
                rfid: [
                ]
            })
        },
        // 在getStation的基础上移除一个station
        removeStation: function(datas){
            datas.pop();
        },
        // 获取所有的rfid的路径
        getRfid: function(){
            return {
                data: [
                    {
                        id: 1,
                        name: 'rfid1',
                        route: [
                            {
                                stationId: 1,
                                time: '13:00'
                            },{
                                stationId: 2,
                                time: '12:30'
                            },{
                                stationId: 3,
                                time: '12:00'
                            }
                        ]
                    },{
                        id: 2,
                        name: 'rfid2',
                        route: [
                            {
                                stationId: 1,
                                time: '13:00'
                            },{
                                stationId: 3,
                                time: '12:30'
                            },{
                                stationId: 2,
                                time: '12:00'
                            }
                        ]
                    },{
                        id: 3,
                        name: 'rfid3',
                        route: [
                            {
                                stationId: 2,
                                time: '13:00'
                            },{
                                stationId: 3,
                                time: '12:40'
                            },{
                                stationId: 1,
                                time: '12:20'
                            }
                        ]
                    },{
                        id: 4,
                        name: 'rfid4',
                        route: [
                            {
                                stationId: 3,
                                time: '13:00'
                            },{
                                stationId: 1,
                                time: '12:40'
                            },{
                                stationId: 2,
                                time: '12:20'
                            }
                        ]
                    }
                ]
            }
        }
    }

    return service;
}

angular
    .module('inspinia')
    .factory('myFactory',myFactory)
    .factory('timestampMarker', ["$rootScope", function ($rootScope) {
        var timestampMarker = {
        request: function (config) {
        $rootScope.loading = true;
        config.requestTimestamp = new Date().getTime();
        return config;
        },
        response: function (response) {
        $rootScope.loading = false;
        response.config.responseTimestamp = new Date().getTime();
        return response;
        }
        };
        return timestampMarker;
    }]);