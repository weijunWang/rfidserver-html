/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
// 主控制器
function MainCtrl($rootScope,$scope,myFactory,$http) {
    this.projectName = '云生物';
    this.helloText = 'Welcome in SeedProject';
    this.descriptionText = 'It is an application skeleton for a typical AngularJS web app. You can use it to quickly bootstrap your angular webapp projects and dev environment for these projects.';

    // 第一次未登录加载
    $scope.locaStorageUserName ="请登录";
    // $scope.student = true;

    // 根据子控制器检查是否登录
    $rootScope.$on("msg",function(e,msg){
        $scope.locaStorageUserName = msg;
    });
    
};
function UsermgrCtrl() {
    this.tt ='ss';
};
// 客户项目列表
function allProList($scope,$http,myFactory,$document){
    // 判断是否登录状态
    myFactory.checkLogin($scope);
    // $scope.hiddenPro = "false";
    // get所有项目
    var id = myFactory.getObjStorage("userInfo").id;
    console.log("id",id);
    $scope.proIndex = parseInt(myFactory.getStorage("proIndex")) || 1;
    console.log("proIndex",$scope.proIndex);
    pagesChange($scope.searchPro);
    function pagesChange(param){
        $http({
            method: "GET",
            url: "service_provider/two_params/"+$scope.proIndex+"/show_projects",
            params: {q: param || ""},
            withCredentials: true,
        }).then(function(response){
            console.log(response);
            for(var i in response.data.data.current_page_data){
                var timer = new Date(response.data.data.current_page_data[i]['create_date']).toLocaleString('chinese',{hour12:false});
                response.data.data.current_page_data[i]['create_date'] = timer.split("/").join("-");
                
                if(response.data.data.current_page_data[i].process_obj.total_cycle){
                    var totalCycle = response.data.data.current_page_data[i].process_obj.total_cycle;
                    var cycleTime = new Date(new Date(response.data.data.current_page_data[i]['create_date']).getTime() + totalCycle * 24 * 60 *60 *1000).toLocaleDateString();
                    response.data.data.current_page_data[i]['totalCycleTime'] = cycleTime.split("/").join("-");

                    var totalCycleTime = response.data.data.current_page_data[i]['totalCycleTime'];
                    var create_date = response.data.data.current_page_data[i]['create_date'];

                    // 进度条
                    if(response.data.data.current_page_data[i].complete){
                        response.data.data.current_page_data[i]['progress'] = {
                            width: 100 + "%"
                        }
                    }else{
                        response.data.data.current_page_data[i]['progress'] = {
                            width: response.data.data.current_page_data[i].process_value + "%" || 0 + "%"
                        };
                    }
                }else{
                    response.data.data.current_page_data[i]['totalCycleTime'] = "未知";
                }
            }
            $scope.proList = response.data.data.current_page_data;
            
            $scope.allProItemPageIndex = response.data.data.num_pages;
            $(".numPage span:first").html($scope.proIndex);
            $(".numPage span:last").html($scope.allProItemPageIndex);
            myFactory.setStorage("proIndex",$scope.proIndex);
            callBackPage($scope);
        },function(response){
            console.log(response);
        });
    }
    // 分页函数
    function callBackPage($scope){
        var allProItemPageIn = 3;
        $scope.prevPage = function(){
            console.log("上一页");
            if($scope.proIndex > 1){
                $scope.proIndex -= 1;
            }
            pagesChange($scope.searchPro);
        };
        $scope.nextPage = function(){
            console.log("下一页");
            if($scope.proIndex < $scope.allProItemPageIndex){
                $scope.proIndex += 1; 
            }
            pagesChange($scope.searchPro);
        };
        $scope.toNumPage = function(){
            console.log("跳页");
            console.log("输入",$(".toPage").find("a>input").val());
            if($(".toPage").find("a>input").val() >=1 && $(".toPage").find("a>input").val() <= $scope.allProItemPageIndex){
                $scope.proIndex = parseInt($(".toPage").find("a>input").val());
            }
            pagesChange($scope.searchPro);
        };
    } 
    $scope.saveProIDDetail = function(){
        var proID = this.pro.id || $(event.target).parent().find(".hiddenID>span").html()
        console.log("proID",proID);
        myFactory.setStorage("proID",proID);
    }
    $scope.saveProID = function(){
        var id0 = this.pro.id;
        var proID = id0 || $(event.target).parent().parent().siblings(".hiddenID").find("span").html() || $(event.target).parent().siblings(".hiddenID").find("span").html();
        console.log("proID",proID);
        myFactory.setStorage("proID",proID);
    }

    // 重新备样
    $scope.againSimple = function(){
        var id = this.pro.id || $(event.target).parent().parent().siblings(".hiddenID").find("span").html() || $(event.target).parent().siblings(".hiddenID").find("span").html();
        console.log("id",id);
        myFactory.setStorage("proID",id);
        $("#TKAgainBY").modal();
        $scope.confirmTKAgain = function(){
            $('#TKAgainBY').modal('hide')
            $http({
                method: "POST",
                url: "service_provider/two_params/"+id+"/reset_project",
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                data: JSON.stringify({inp: "search"}),
                withCredentials: true,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            }).then(function(response){
                console.log(response);
                if(response.data.success){
                    // alert("已经重新备样");
                    setTimeout(function(){
                        location.href = "#/index/customer/createSimple";
                    },500);
                }
            },function(response){
                console.log(response);
            });
        }
    }
    $scope.TKUploadPro = function(){
        var id0 = this.pro.id;
        var proID = id0 || $(event.target).parent().parent().siblings(".hiddenID").find("span").html() || $(event.target).parent().siblings(".hiddenID").find("span").html();
        console.log("proID",proID);
        myFactory.setStorage("proID",proID);
        location.href = "#/index/yunSW/proData";
    }
    // 搜索项目按钮
    $scope.searchProBtn = function(){
        console.log($scope.searchPro);
        $scope.proIndex = 1;
        pagesChange($scope.searchPro);
    }
    $document.bind("keydown",function(event){
        if(event.keyCode == 13){
            var url = event.currentTarget.URL;
            url = url.split("/");
            url = url[url.length - 1];
            console.log(url);
            if(url == "allProList"){
                $scope.searchProBtn();
            }
        }
    })
    // 确认项目报告弹框
    $scope.TK1 = function(){
        console.log("项目id",$(".TKHiddenID").find("span").html());
        $http({
            method: "GET",
            url: "service_provider/two_params/"+$(".TKHiddenID").find("span").html()+"/show_report",
            withCredentials: true,
        }).then(function(response){
            console.log(response);
            if(response.data.data[0]){
                $scope.hasFile = true;
                response.data.data[0].addr = "" + response.data.data[0].addr.split("/").slice(3).join("/")
                $scope.fileAddr = response.data.data[0].addr;
                console.log($scope.fileAddr);
                $scope.fileName = $scope.fileAddr.split("/");
                $scope.fileName = $scope.fileName[$scope.fileName.length - 1];
                console.log("报告名字",$scope.fileName);
            }else{
                $scope.hasFile = false;
            }
        });
        
    }
    // 文件下载
    $scope.downLoad = function(){
        var a = document.createElement('a');  
        document.body.appendChild(a);  
        a.setAttribute('style', 'display:none');  
        a.setAttribute('href', $scope.fileAddr);  
        a.setAttribute('download','');  
        a.click();
    }
    // 弹框提交
    $scope.TKConfirm = function(){
        console.log(myFactory.getTKProData($scope));
        console.log("项目id",$(".TKHiddenID").find("span").html());
        $http({
            method: "POST",
            url: "service_provider/two_params/"+$(".TKHiddenID").find("span").html()+"/report_confirm",
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: myFactory.getTKProData($scope), 
            withCredentials: true,
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        }).then(function(response){
            console.log(response);
            history.go(0);
        },function(response){
            console.log(response);
        });
    }
    // 服务商评价
    // 星级评价星星的个数
    $scope.max = 5;
    $scope.svEvaCli = function(){
        $scope.service_provider_id = this.pro.service_provider_id;
        $scope.proId = this.pro.id || $(".TKHiddenID").find("span").html();
        console.log("服务商评价",$scope.proId);
        // choiceStars($scope.service_star)
        //星星等级评分
        $scope.service_star = 0;
        $scope.hoverVal = 0;
        $scope.onHover = function(val) {
            $scope.hoverVal = val;
        };
        $scope.onLeave = function() {
            $scope.hoverVal = $scope.service_star;
        }
        $scope.onChange = function(val) {
            $scope.service_star = val;
        }
    }
    $scope.TKSvEvaluate = function(){
        console.log(myFactory.getSvEvaData($scope));
        console.log("svId",$scope.service_provider_id);
        console.log("proId",$scope.proId);
        $http({
            method: "POST",
            url: "service_provider/three_params/"+$scope.proId+"/"+$scope.service_provider_id+"/save_evaluation",
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: myFactory.getSvEvaData($scope), 
            withCredentials: true,
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        }).then(function(response){
            console.log(response);
            if(response.data.success){
                // alert("已评价");
                history.go(0);
            }else{
                // alert("评价失败");
            }
        },function(response){
            console.log(response);
        });
    }
    // 物流评价
    $scope.wlEvaCli = function(){
        $scope.shipment_enterprise_id = this.pro.shipment_enterprise_id;
        $scope.proId = this.pro.id || $(".TKHiddenID").find("span").html();
        console.log("物流评价",$scope.proId);
        //星星等级评分
        $scope.logistics_star = 0;
        $scope.hoverVal = 0;
        $scope.onHover = function(val) {
            $scope.hoverVal = val;
        };
        $scope.onLeave = function() {
            $scope.hoverVal = $scope.logistics_star;
        }
        $scope.onChange = function(val) {
            $scope.logistics_star = val;
        }
    }
    $scope.TKWlEvaluate = function(){
        console.log(myFactory.getWlEvaData($scope));
        console.log("wlId",$scope.shipment_enterprise_id);
        console.log("proId",$scope.proId);
        $http({
            method: "POST",
            url: "service_provider/three_params/"+$scope.proId+"/"+$scope.shipment_enterprise_id+"/save_evaluation",
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: myFactory.getWlEvaData($scope), 
            withCredentials: true,
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        }).then(function(response){
            console.log(response);
            if(response.data.success){
                // alert("已评价");
                history.go(0);
            }else{
                // alert("评价失败");
            }
        },function(response){
            console.log(response);
        });
    }
}
angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)
    .controller('UserMgrCtrl',UsermgrCtrl)
    .controller('allProList',allProList)




    // 测试代码
    .controller("testCon", function($scope,$http,myFactory){
                // 分片文件上传
                // $scope.fileSlice = function(){
                //     var blob = new Blob(["hellow"]);

                //     var a = document.createElement("a");
                //     a.href = window.URL.createObjectURL(blob);
                //     a.download = "test0000.txt";
                //     a.textContent = "download hellow";
                //     document.body.appendChild(a);
                //     a.click();
                // }
                // // 文件下载
                // $scope.getExcel = function(){
                //     $http({
                //         url: "./testDownlod/12.png",
                //         headers: {  
                //             'Content-type': 'application/json'  
                //         }, 
                //         method: 'get',
                //         responseType: 'arraybuffer'
                //     }).then(function(response){
                //         var blob = new Blob([response.data], {type: "application/vnd.ms-excel"});  
                //         var objectUrl = URL.createObjectURL(blob);  
                //         var a = document.createElement('a');  
                //         document.body.appendChild(a);  
                //         a.setAttribute('style', 'display:none');  
                //         a.setAttribute('href', objectUrl);  
                //         var filename="测试下载.png";  
                //         a.setAttribute('download', filename);  
                //         a.click();  
                //         URL.revokeObjectURL(objectUrl); 
                //     });
                // }
                // $scope.postCli = function(){
                //     $http({
                //         method: "POST",
                //         url: "/add/",
                //         headers: {  
                //             'Content-type': 'application/json'  
                //         }, 
                //         data: 1,
                //         transformResponse: function(data, headers){  
                //             //MESS WITH THE DATA  
                //             var response = {};  
                //             response.data = data;  
                //             return response;  
                //         }
                //     }).then(function(response){
                //         console.log(response);
                //     });
                // }
                // $scope.getCli = function(){
                //     $http({
                //         method: "GET",
                //         url: "/add/"
                //     }).then(function(response){
                //         console.log(response);
                //     });
                // }
                // $scope.putCli = function(){
                //     $http({
                //         method: "PUT",
                //         url: "/add/",
                //         data: {name:"name"}
                //     }).then(function(response){
                //         console.log(response);
                //     });
                // }
                // $scope.deleteCli = function(){
                //     $http({
                //         method: "DELETE",
                //         url: "/add/",
                //         data: {name:"name"}
                //     }).then(function(response){
                //         console.log(response);
                //     });
                // }
                // $scope.fileCli = function(){
                //     console.log($scope.upload_files)
                //     console.log(myFactory.formatFils($scope))
                //     var datas = {
                //             save: JSON.stringify({name:1}),
                //             0: $scope.upload_files[0],
                //             1: $scope.upload_files[1]
                //         }
                //     $http({
                //         method: "POST",
                //         url: "/add/",
                //         headers: {
                //             'Content-Type': 'multipart/form-data'
                //         },
                //         data: datas,
                //         transformRequest: function (data, headersGetter) {
                //             var formData = new FormData();
                //             angular.forEach(data, function (value, key) {
                //                 formData.append(key, value);
                //             });

                //             var headers = headersGetter();
                //             delete headers['Content-Type'];

                //             return formData;
                //         }
                //     }).then(function(response){
                //         console.log(response);
                //     });
                // }
                // $scope.base64Jia = function(){
                //     console.log("加密");
                //     var val = $(".base64Test").val();
                //     var result = Base64.encode(val);
                //     console.log(result);
                // }
                // $scope.base64Jie = function(){
                //     console.log("解密");
                //     var val = $(".base64Test").val();
                //     var result = Base64.decode(val);
                //     console.log(result);
                // }
           })
