/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider,$httpProvider) {
    $httpProvider.interceptors.push('timestampMarker');
    $urlRouterProvider.otherwise("index/rfidHtml");
    var _lazyLoad = function(loaded) {
        return function($ocLazyLoad) {
            return $ocLazyLoad.load(loaded, {
                serie: true,
            });
        };
    };
    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "../../static/front/views/common/content.html",
            data: { pageTitle: 'Y.SYS' }
        })
        
        .state('index.main', {
            url: "/main",
            templateUrl: "../../static/front/views/main.html",
            data: { pageTitle: 'Example view' }
        })
        .state('index.minor', {
            url: "/minor",
            templateUrl: "../../static/front/views/minor.html",
            data: { pageTitle: 'Example view' }
        })
        .state('index.rfidHtml',{
            url: '/rfidHtml',
            templateUrl: '../../static/front/views/rfidHtml.html',
            controller: 'RfidHtmlCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/rfidHtml.js'),
            },
        })
        //
        .state("index.AjaxTest",{
            url: "/AjaxTest",
            templateUrl: "../../static/front/views/AjaxTest.html"
        })
}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
