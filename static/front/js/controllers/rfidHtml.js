
angular
    .module('inspinia')
    .controller('RfidHtmlCtrl', RfidHtmlCtrl)
function RfidHtmlCtrl($scope, $rootScope, myFactory) {
    console.log('RfidHtmlCtrl');

    $scope.number = 1;
    $scope.allStations = myFactory.getStation().data;
    for(var i = 0;i < $scope.allStations.length;i++){
        $scope.allStations[i].top = 1;
        $scope.allStations[i].left = 1;
    }
    $scope.checkStation = function ($event) {
        console.log($($event.target).offset())
        // console.log(this);
        for (var i = 0; i < $scope.allStations.length; i++) {
            if ($scope.allStations[i].name == this.sta.name) {
                $scope.allStations[i].left = $($event.target).offset().left + 50;
                $scope.allStations[i].top = $($event.target).offset().top + 50;
            }
        }
    }

    $(function () {
        var canvas = document.getElementById('canvas');
        var div = document.getElementById('div');
        var ctx = canvas.getContext("2d");
        canvas.height = div.clientHeight;
        canvas.width = div.clientWidth;
        console.log(div.clientHeight);
        var myDrag = document.getElementsByClassName('dragDiv')
        ctx.fillStyle = "#fff";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        $scope.checkRfid = function ($event) {
            console.log(this);
            var route = myFactory.getRfid().data[this.rfid.id - 1].route;
            // console.log(myFactory.getRfid().data[this.rfid.id - 1]);
            if (route.length >= 2) {
                console.log(1);
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                console.log(2);
                for (var i = route.length - 1; i >= 0; i--) {
                    if(i > 0){
                        var x1 = $scope.allStations[route[i].stationId - 1].left;
                        var y1 = $scope.allStations[route[i].stationId - 1].top;
                        var x2 = $scope.allStations[route[i - 1].stationId - 1].left;
                        var y2 = $scope.allStations[route[i - 1].stationId - 1].top;
                        drawArrow(ctx, x1, y1, (x2 - x1) / 2 + x1, (y2 - y1) / 2 + y1, 30, 20, 1, '#000')
                        drawArrow(ctx, (x2 - x1) / 2 + x1, (y2 - y1) / 2 + y1, x2, y2, 30, 20, 1, '#000')
                    }
                }
                console.log(3);
            }
            $event.stopPropagation();
        }
        function drawArrow(ctx, fromX, fromY, toX, toY, theta, headlen, width, color) {
            theta = typeof (theta) != 'undefined' ? theta : 30;
            headlen = typeof (theta) != 'undefined' ? headlen : 10;
            width = typeof (width) != 'undefined' ? width : 1;
            color = typeof (color) != 'color' ? color : '#000';

            // 计算各角度和对应的P2,P3坐标
            var angle = Math.atan2(fromY - toY, fromX - toX) * 180 / Math.PI,
                angle1 = (angle + theta) * Math.PI / 180;
            angle2 = (angle - theta) * Math.PI / 180;
            topX = headlen * Math.cos(angle1);
            topY = headlen * Math.sin(angle1);
            botX = headlen * Math.cos(angle2);
            botY = headlen * Math.sin(angle2);
            ctx.save();
            ctx.beginPath();

            var arrowX = fromX - topX,
                arrowY = fromY - topY;
            ctx.moveTo(arrowX, arrowY);
            ctx.moveTo(fromX, fromY);
            ctx.lineTo(toX, toY);
            arrowX = toX + topX;
            arrowY = toY + topY;
            ctx.moveTo(arrowX, arrowY);
            ctx.lineTo(toX, toY);
            arrowX = toX + botX;
            arrowY = toY + botY;
            ctx.lineTo(arrowX, arrowY);
            ctx.strokeStyle = color;
            ctx.lineWidth = width;
            ctx.stroke();
            ctx.restore();
        }
        $("#change-station-button").css('display', 'block');
        $("#add-station-button").css('display', 'none');
        $("#remove-station-button").css('display', 'none');
        $("#confirm-station-button").css('display', 'none');
        $("#add-station-button").click(function () {
            myFactory.addStation($scope.allStations)
            $scope.$apply();
            if (myDrag.length > 0) {
                for (var i = 0; i < myDrag.length; i++) {
                    bindDrag(myDrag[i], true);
                }
            }
        })
        $("#remove-station-button").click(function () {
            myFactory.removeStation($scope.allStations)
            $scope.$apply();
            console.log($scope.allStations);
            if (myDrag.length > 0) {
                for (var i = 0; i < myDrag.length; i++) {
                    bindDrag(myDrag[i], true);
                }
            }
        })
        $("#change-station-button").click(function () {
            $("#add-station-button").css('display', 'block')
            $("#remove-station-button").css('display', 'block')
            $("#change-station-button").css('display', 'none');
            $("#confirm-station-button").css('display', 'block');
            if (myDrag.length > 0) {
                for (var i = 0; i < myDrag.length; i++) {
                    bindDrag(myDrag[i], true);
                }
            }
        })
        $("#confirm-station-button").click(function () {
            $("#add-station-button").css('display', 'none')
            $("#remove-station-button").css('display', 'none')
            $("#change-station-button").css('display', 'block');
            $("#confirm-station-button").css('display', 'none');
            if (myDrag.length > 0) {
                for (var i = 0; i < myDrag.length; i++) {
                    bindDrag(myDrag[i], false);
                }
            }
        })
    })
}